package com.theironyard.timeliness;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimelinessApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimelinessApplication.class, args);
	}
}
