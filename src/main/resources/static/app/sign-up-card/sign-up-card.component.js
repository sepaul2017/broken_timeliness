class SignUpCardController {
	  constructor($http, $state) { // New parameter
		    this.username = '';
		    this.password = '';
		    this.$http = $http;
		    this.$state = $state; // Save the service
		  }


  submitForm() {
	  // Create an object to send to the API endpoint
	  // for logging in
	  let credentials = {
	    username: this.username,
	    password: this.password
	  }

	  // Use AJAX to PUT the credentials to the API
	  this.$http
	    .post('/api/users', credentials)
	    .then(() => {

	      // Show SUCCEEDED if all goes well!
	    	this.$state.location.href = '/';
	    })
	    .catch(() => {

	      // Show NO LOGIN if all goes poorly.
	        this.error = 'Cannot login with that username and password';

	    });
	}

}

angular
.module('app')
.component('signUpCard', {
  templateUrl: '/app/sign-up-card/sign-up-card.component.html',
  controllerAs: 'signup',
  controller: [
    '$http',

    '$state', // Ask for the $state service

    // Include it in the parameter list for your
    // controller
    ($http, $state) => new SignUpCardController($http, $state)
  ]
});


